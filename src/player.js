import React from 'react';
import './player.css';
import {Player as Video} from 'video-react';
import "../node_modules/video-react/dist/video-react.css"; // import css
import $ from 'jquery'

var axios = require('axios');
var createReactClass = require('create-react-class');
var renderingTimeInMs = 1000; // 1 second
var baseServerAddress = "https://nodesenior.azurewebsites.net/player";
var playerServerAddress;

var Player = createReactClass({

    getInitialState: function() {
        // SYNCHRONOUS Ajax call. Our app cannot call readDataFromServer without playerId,
        // so there is no point to proceed before getting this data
        $.ajax({
            type: "GET",
            url: `${baseServerAddress}/all`,
            async: false,
            success: (data) => {
                var playerId = JSON.parse(data)[0];
                playerServerAddress = `${baseServerAddress}/${playerId}`;
                this.readDataFromServer();
            },
            error: (err) => {
                console.error(`calling ${baseServerAddress}/all failed`)
            }
        });
        return {secondsElapsed: 0};
    },

    // will be executed immediately after render function is committed
    componentDidMount: function() {
        this.readDataFromServer();
        // periodical update from server
        setInterval(this.readDataFromServer, renderingTimeInMs);
    },

    // this function checks what fields user changed, and sends only them as an update
    getManualUpdates: function() {
        let updates = {};
        if (this.state.nameManual) {
            addToObject(updates, ['player','name'], this.state.nameManual);
        }
        if (this.state.picUrlManual) {
            addToObject(updates, ['player', 'picUrl'], this.state.picUrlManual);
        }
        if (this.state.pointsManual) {
            addToObject(updates, ['statistics', 'points'], this.state.points);
        }
        if (this.state.reboundsManual) {
            addToObject(updates, ['statistics', 'rebounds'], this.state.rebounds);
        }
        if (this.state.clipManual) {
            addToObject(updates, ['statistics', 'lastgame', 'game_highlights'], this.state.game_highlights);
        }
        return updates;

        // this function put value in inner field
        function addToObject(object, fieldArray, newValue) {
            while (fieldArray.length > 1) {
                let field = fieldArray.shift();
                if (object[field] === undefined) {
                    object[field] = {};
                }
                object = object[field]
            }
            object[fieldArray[0]] = newValue;
        }
    },

    // sending updates to the server
    handleClick: function() {
        let updates = this.getManualUpdates();
        axios.post(playerServerAddress, updates).then((res)=> {
            console.log("Server has been updated with the new data: " + JSON.stringify(updates));
        }, (err) => {
            console.error(`Posting updates to ${playerServerAddress} failed`);
        })
    },

    handleChange: function(event) {
        let propertyName = event.target.name;
        this.state[propertyName + 'Manual'] = event.target.value;
        this.state[propertyName] = event.target.value;
        this.setState({propertyName: event.target.value});
        event.preventDefault();
    },

    render: function() {
        return (
            <div className="player-div">
                <div className="square navy"/> <h4>Player</h4>
                <div className="container">
                <div className="container"> <div className="square blue"/> <h4>Name</h4>
                    <input className="long-input" value={this.state.name} onChange={this.handleChange} type="text"
                           name="name"/>
                </div>
                <div className="container"> <div className="square blue"/> <h4>Image</h4>
                    <input className="long-input" type="text" value={this.state.picUrl} onChange={this.handleChange}
                           name="picUrl"/>
                </div>
                <div className="container"> <img className="playerImage" src={this.state.picUrl}/></div>
                <div className="container"> <div className="square blue"/> <h4>Statistics</h4>
                    <div className="container">
                        <div className="square light-blue"/> <h4>Points</h4>
                        <input type="number" className="short-input" value={this.state.points} onChange={this.handleChange}
                               name="points" />
                    </div>
                    <div className="container">
                        <div className="square light-blue"/> <h4>Rebounds</h4>
                        <input className="short-input" type="number" value={this.state.rebounds} onChange={this.handleChange}
                               name="rebounds"/>
                    </div>
                </div>
                <div className="container">
                    <div className="square blue"/> <h4>Last Game</h4>
                    <div className="container">
                        <div className="square light-blue"/> <h4>Clip</h4>
                        <input className="long-input" type="text" value={this.state.clip} onChange={this.handleChange}
                               name="clip"/>
                    </div>
                </div>
                <div className="container video-container">
                    <Video playsInline src={this.state.clip} />
                </div>
                <button className="submit-btn" onClick={this.handleClick}> SAVE </button>
                </div>
            </div>
        )
    },

    readDataFromServer: function() {
        axios.get(playerServerAddress).then((res) => {
            // every property has "server" propery and "manual" property. We give priority to the manual property
            // over the server one.
            this.setState({name: this.state.nameManual || `${res.data.player.firstName} ${res.data.player.lastName}`});
            this.setState({picUrl: this.state.picUrlManual || res.data.player.picUrl});
            this.setState({points: this.state.pointsManual || parseInt(res.data.statistics.points)});
            this.setState({rebounds: this.state.reboundsManual || parseInt(res.data.statistics.rebounds)});
            this.setState({clip: this.state.clipManual || res.data.statistics.lastgame.game_highlights});
        }, (err) => {
            console.error(`Get data from ${playerServerAddress} failed`);
        })
    }
})

export default Player;